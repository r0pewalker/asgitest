FROM python:3.7

WORKDIR /opt/app/

COPY requirements.txt /opt/app/

RUN pip install -r requirements.txt

COPY . /opt/app

EXPOSE 8000
